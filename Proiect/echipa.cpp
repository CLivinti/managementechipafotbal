#include<iostream>
#include<fstream>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include "echipa.h"
#include "persoana.h"
using namespace std;

int Echipa::nrEchipe=0;
int Stadion::nrStadioane=0;
int AngajatEchipa::nrAngajati=0;

Echipa::Echipa()
{
	numeEchipa=nullptr;
	numeCampionat=nullptr;
	buget=0;
	punctaj=0;
	pozitieClasament=0;
	++nrEchipe;
}
Echipa::Echipa(char *nE,char *nC,double bug,int punc,int pozCl)
{
	setNumeEchipa(nE);
	setNumeCampionat(nC);
	setBuget(bug);
	setPuncte(punc);
	setPozitieClasament(pozCl);
	++nrEchipe;
}

Echipa::Echipa(const Echipa &e)
{
	setNumeEchipa(e.numeEchipa);
	setNumeCampionat(e.numeCampionat);
	setBuget(e.buget);
	setPuncte(e.punctaj);
	setPozitieClasament(e.pozitieClasament);
	++nrEchipe;
}

Echipa::~Echipa()
{
	uitaSir(numeEchipa);
	uitaSir(numeCampionat);
	--nrEchipe;
}

int Echipa::getNrEchipe()
{
	return nrEchipe;
}

void Echipa::setNrEchipe(int nr)
{
	this->nrEchipe=nr;
}

char* Echipa::getNumeEchipa()
{
	return numeEchipa;
}

void Echipa::setNumeEchipa(char *nE)
{
	numeEchipa=copiereSir(nE);
}

char* Echipa::getNumeCampionat()
{
	return numeCampionat;
}

void Echipa::setNumeCampionat(char* nC)
{
	numeCampionat=copiereSir(nC);
}

double Echipa::getBuget()
{
	return buget;
}

void Echipa::setBuget(double bug)
{
	this->buget=bug;
}

int Echipa::getPuncte()
{
	return punctaj;
}

void Echipa::setPuncte(int punct)
{
	this->punctaj=punct;
}

int Echipa::getPozitieClasament()
{
	return pozitieClasament;
}

void Echipa::setPozitieClasament(int pozCl)
{
	this->pozitieClasament=pozCl;
}

void Echipa::eroare(int cond,char* mesaj)
{
	if(cond)
	{
		cout<<mesaj;
		exit(EXIT_FAILURE);
	}
}

char* Echipa::citireSir(char* mesaj)
{
	char buffer[MAXN], *sir=nullptr;
	cout<<mesaj;
	cin.ignore(MAXN,'\n');
	cin.getline(buffer,MAXN-2);
	sir=copiereSir(buffer);
	return sir;
}

int Echipa::citireInt(char *mesaj)
{
	int x,ok;
	char buffer[MAXN];
	do{
		cout<<mesaj;
		cin.getline(buffer,MAXN-1,'\n');
		ok=sscanf_s(buffer,"%d",&x);
		if(ok!=1)
		{
			ok=0;
		}
	}while(!ok);
	return x;
}

double Echipa::citireReal(char* mesaj)
{
	double x;
	int ok;
	char buffer[MAXN];
	do{
		cout<<mesaj;
		cin.getline(buffer,MAXN-1,'\n');
		ok=sscanf_s(buffer,"%lf",&x);
		if(ok!=1)
		{
			ok=0;
		}
	}while(!ok);
	return x;
}

void Echipa::uitaSir(char *&sir)
{
	if(sir)
	{
		delete[] sir;
		sir=0;
	}
}

char* Echipa::copiereSir(char* sir)
{
	char *s=nullptr;
	if(sir)
	{
		s=new char[strlen(sir)+1];
		eroare(!s,"Eroare la alocare!");
		strcpy_s(s,strlen(sir)+1,sir);
	}
	return s;
}

char* Echipa::citireSir(istream& fin)
{
	char buffer[MAXN];
	char *sir=nullptr;
	fin.getline(buffer,MAXN-1);
	sir=copiereSir(buffer);
	return sir;
}

int Echipa::citireInt(istream &fin)
{
	char buffer[MAXN];
	int x;
	fin.getline(buffer,MAXN-1);
	sscanf_s(buffer,"%d",&x);
	return x;
}

void Echipa::citire()
{
	char *NE,*NC;
	double BUG;
	int POZ,PUNCTE;
	NE=Echipa::citireSir("Numele echipei:");
	setNumeEchipa(NE);
	NC=Echipa::citireSir("Numele campionatului:");
	setNumeCampionat(NC);
	cout<<"Buget echipa:";
	cin>>BUG;
	setBuget(BUG);
	cout<<"Pozitie clasament:";
	cin>>POZ;
	setPozitieClasament(POZ);
	cout<<"Puncte obtinute:";
	cin>>PUNCTE;
	setPuncte(PUNCTE);
}

void Echipa::citire(istream &fin)
{
	numeEchipa=citireSir(fin);
	numeCampionat=citireSir(fin);
	fin>>buget;
	fin.get();
	fin>>pozitieClasament;
	fin.get();
	fin>>punctaj;
	fin.get();
}

void Echipa::afisare()
{
	cout<<this->numeEchipa<<endl;
	cout<<this->numeCampionat<<endl;
	cout<<"Buget:"<<this->buget<<endl;
	cout<<"Pozitie in clasament:"<<this->pozitieClasament<<" cu "<<this->punctaj<<" puncte obtinute."<<endl;
}

void Echipa::afisare(ostream &fout)
{
	fout<<this->numeEchipa<<endl;
	fout<<this->numeCampionat<<endl;
	fout<<"Buget:"<<this->buget<<endl;
	fout<<"Pozitie in clasament:"<<this->pozitieClasament<<" cu "<<this->punctaj<<" puncte obtinute."<<endl;
}

Stadion::Stadion()
{
	numeStadion=nullptr;
	nrLocuri=0;
	lungimeTeren=0;
	latimeTeren=0;
	++nrStadioane;
}

Stadion::Stadion(char* nS,int nrLoc,double lung,double lat)
{
	setNumeStadion(nS);
	setLocuri(nrLoc);
	setLungime(lung);
	setLatime(lat);
	++nrStadioane;
}

Stadion::Stadion(const Stadion &s) :Echipa(s.numeEchipa, s.numeCampionat, s.buget, s.punctaj, s.pozitieClasament)
{
	setNumeStadion(s.numeStadion);
	setLocuri(s.nrLocuri);
	setLungime(s.lungimeTeren);
	setLatime(s.latimeTeren);
	++nrStadioane;
}

Stadion::Stadion(char* nS,int nrLoc,double lung,double lat,char* nE,char* nC,double bug,int punct,int pozCl) :Echipa(nE, nC, bug, punct, pozCl)
{
	setNumeStadion(nS);
	setLocuri(nrLoc);
	setLungime(lung);
	setLatime(lat);
	++nrStadioane;
}

Stadion::~Stadion()
{
	uitaSir(numeStadion);
	--nrStadioane;
}

int Stadion::getNrStadioane()
{
	return nrStadioane;
}

void Stadion::setNrStadioane(int nr)
{
	this->nrStadioane=nr;
}

char* Stadion::getNumeStadion()
{
	return this->numeStadion;
}

void Stadion::setNumeStadion(char* nS)
{
	numeStadion=copiereSir(nS);
}

int Stadion::getLocuri()
{
	return nrLocuri;
}

void Stadion::setLocuri(int nrLoc)
{
	this->nrLocuri=nrLoc;
}

double Stadion::getLungime()
{
	return lungimeTeren;
}

void Stadion::setLungime(double lung)
{
	this->lungimeTeren=lung;
}

double Stadion::getLatime()
{
	return latimeTeren;
}


void Stadion::setLatime(double lat)
{
	this->latimeTeren=lat;
}

void Stadion::citire()
{
	//Echipa::citire();
	char *NS;
	int nrLoc;
	double lung,lat;
	NS=Echipa::citireSir("Numele Stadionului:");
	setNumeStadion(NS);
	cout<<"Numarul de locuri:";
	cin>>nrLoc;
	setLocuri(nrLoc);
	cout<<"Lungime teren de joc:";
	cin>>lung;
	setLungime(lung);
	cout<<"Latime teren de joc:";
	cin>>lat;
	setLatime(lat);
}

void Stadion::citire(istream &fin)
{
	numeStadion=citireSir(fin);
	fin>>nrLocuri;
	fin.get();
	fin>>lungimeTeren;
	fin.get();
	fin>>latimeTeren;
	fin.get();
	Echipa::citire(fin);
}

void Stadion::afisare()
{
	//Echipa::afisare();
	cout<<this->numeStadion<<":"<<this->nrLocuri<<endl;
	cout<<this->lungimeTeren<<"X"<<this->latimeTeren<<endl;
}

void Stadion::afisare(ostream &fout)
{
	//Echipa::afisare(fout);
	fout<<this->numeStadion<<":"<<this->nrLocuri<<endl;
	fout<<this->lungimeTeren<<"X"<<this->latimeTeren<<endl;
}

Meciuri::Meciuri(int vic,int egal,int infrang,int golM,int golP)
{
	setVictorii(vic);
	setEgaluri(egal);
	setInfrangeri(infrang);
	setGoluriM(golM);
	setGoluriP(golP);
}

Meciuri::Meciuri(const Meciuri &m) :Echipa(m.numeEchipa, m.numeCampionat, m.buget, m.punctaj, m.pozitieClasament)
{
	setVictorii(m.victorii);
	setEgaluri(m.egaluri);
	setInfrangeri(m.infrangeri);
	setGoluriM(m.goluriMarcate);
	setGoluriP(m.goluriPrimite);
}

Meciuri::Meciuri(int vic,int egal,int infrang,int golM,int golP,char* nE,char* nC,double bug,int punct,int pozCl) :Echipa(nE, nC, bug, punct, pozCl)
{
	setVictorii(vic);
	setEgaluri(egal);
	setInfrangeri(infrang);
	setGoluriM(golM);
	setGoluriP(golP);
}

int Meciuri::getVictorii()
{
	return victorii;
}

void Meciuri::setVictorii(int vic)
{
	this->victorii=vic;
}

int Meciuri::getEgaluri()
{
	return egaluri;
}

void Meciuri::setEgaluri(int egal)
{
	this->egaluri=egal;
}

int Meciuri::getInfrangeri()
{
	return infrangeri;
}

void Meciuri::setInfrangeri(int infrang)
{
	this->infrangeri=infrang;
}

int Meciuri::getGoluriM()
{
	return goluriMarcate;
}

void Meciuri::setGoluriM(int golM)
{
	this->goluriMarcate=golM;
}

int Meciuri::getGoluriP()
{
	return goluriPrimite;
}

void Meciuri::setGoluriP(int golP)
{
	this->goluriPrimite=golP;
}

void Meciuri::citire()
{
	//Echipa::citire();
	int vic,egal,inf,gm,gp;
	cout<<"Victorii:";
	cin>>vic;
	setVictorii(vic);
	cout<<"Egaluri:";
	cin>>egal;
	setEgaluri(egal);
	cout<<"Infrangeri:";
	cin>>inf;
	setInfrangeri(inf);
	cout<<"Goluri marcate:";
	cin>>gm;
	setGoluriM(gm);
	cout<<"Goluri primite:";
	cin>>gp;
	setGoluriP(gp);
}

void Meciuri::citire(istream &fin)
{
	fin>>victorii;
	fin.get();
	fin>>egaluri;
	fin.get();
	fin>>infrangeri;
	fin.get();
	fin>>goluriMarcate;
	fin.get();
	fin>>goluriPrimite;
	fin.get();
	Echipa::citire(fin);
}

void Meciuri::afisare()
{
	//Echipa::afisare();
	cout<<"V\tE\tI"<<endl;
	cout<<this->victorii<<"\t"<<this->egaluri<<"\t"<<this->infrangeri<<endl;
	cout<<"GM-GP"<<endl;
	cout<<this->goluriMarcate<<"-"<<this->goluriPrimite<<endl;
}

void Meciuri::afisare(ostream &fout)
{
	//Echipa::afisare(fout);
	fout<<"V\tE\tI"<<endl;
	fout<<this->victorii<<"\t"<<this->egaluri<<"\t"<<this->infrangeri<<endl;
	fout<<"GM-GP"<<endl;
	fout<<this->goluriMarcate<<"-"<<this->goluriPrimite<<endl;
}

AngajatEchipa::AngajatEchipa()
{
	ocupatie=nullptr;
	salariu=0;
	dataAngajarii=nullptr;
	++nrAngajati;
}

AngajatEchipa::AngajatEchipa(char* pozEc,double sal,Data *d)
{
	setOcupatie(pozEc);
	setSalariu(sal);
	setData(d);
	++nrAngajati;
}

AngajatEchipa::AngajatEchipa(const AngajatEchipa &ang) :Echipa(ang.numeEchipa,ang.numeCampionat,ang.buget,ang.punctaj,ang.pozitieClasament),Persoana(ang.nume,ang.varsta)
{
	setOcupatie(ang.ocupatie);
	setSalariu(ang.salariu);
	setData(ang.dataAngajarii);
	++nrAngajati;
}

AngajatEchipa::AngajatEchipa(char* poz,double sal,Data *d,char* nume,int varsta,char* nE,char* nC,double bug,int punct,int pozCl)
	:Echipa(nE,nC,bug,punct,pozCl),Persoana(nume,varsta)
{
	setOcupatie(poz);
	setSalariu(sal);
	setData(d);
	++nrAngajati;
}

AngajatEchipa::~AngajatEchipa()
{
	uitaSir(ocupatie);
	this->dataAngajarii=NULL;
	--nrAngajati;
}

int AngajatEchipa::getNrAngajati()
{
	return nrAngajati;
}

void AngajatEchipa::setNrAngajati(int nr)
{
	this->nrAngajati=nr;
}

char* AngajatEchipa::getOcupatie()
{
	return ocupatie;
}

void AngajatEchipa::setOcupatie(char* ocup)
{
	ocupatie=copiereSir(ocup);
}

double AngajatEchipa::getSalariu()
{
	return salariu;
}

void AngajatEchipa::setSalariu(double sal)
{
	this->salariu=sal;
}

Data* AngajatEchipa::getData()
{
	return dataAngajarii;
}

void AngajatEchipa::setData(Data *d)
{
	this->dataAngajarii=new Data(*d);
}

void AngajatEchipa::citire()
{
	Persoana::citire();
	char *ocup;
	double sal;
	do{
		ocup=Echipa::citireSir("Ocupatia:");
		setOcupatie(ocup);
	}while(strcmp(ocup,"Antrenor")!=0 && strcmp(ocup,"Medic")!=0 && strcmp(ocup,"Jucator")!=0 && strcmp(ocup,"antrenor")!=0 && strcmp(ocup,"medic")!=0 && strcmp(ocup,"jucator")!=0);
	cout<<"Salariu:";
	cin>>sal;
	setSalariu(sal);
	int z,l,a;
	do{
		cout<<"Ziua Angajarii:";
		cin>>z;
	}while(z<1 && z>31);
	do{
		cout<<"Luna Angajarii:";
		cin>>l;
	}while(l<1 && l>12);
	do{
		cout<<"Anul Angajarii:";
		cin>>a;
	}while(a<2000 && a>2015);
	this->dataAngajarii=new Data(z,l,a);
	//Echipa::citire();
}

void AngajatEchipa::citire(istream &fin)
{
	Persoana::citire(fin);
	ocupatie=citireSir(fin);
	fin>>salariu;
	fin.get();
	int z,l,a;
	fin>>z;
	fin>>l;
	fin>>a;
	dataAngajarii=new Data(z,l,a);
	//Echipa::citire(fin);
}

void AngajatEchipa::afisare()
{
	Persoana::afisare();
	cout<<this->ocupatie<<" "<<this->salariu<<endl;
	cout<<"Angajat la data: "<<this->dataAngajarii->getZi()<<"."<<this->dataAngajarii->getLuna()<<"."<<this->dataAngajarii->getAn()<<endl;
	Echipa::afisare();
}

void AngajatEchipa::afisare(ostream &fout)
{
	Persoana::afisare(fout);
	fout<<this->ocupatie<<" "<<this->salariu<<endl;
	fout<<"Angajat la data: "<<this->dataAngajarii->getZi()<<"."<<this->dataAngajarii->getLuna()<<"."<<this->dataAngajarii->getAn()<<endl;
	Echipa::afisare(fout);
}
