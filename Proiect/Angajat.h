#pragma once

#include "echipa.h"
#include "persoana.h"

class Jucator:public AngajatEchipa
{
	static int nrJucatori;
protected:
	char* pozitieTeren;
	char* specialitatea; //Tehnic,Puternic,Rapid,Imprevizibil,Cap,Recuperare;
	double inaltime;
	double viteza; // km/ora;
	double detentaInaltime;
	double procentForma; // Fizica si psihica, procentaj intre 0% si 100%;
public:
	Jucator();
	Jucator(char *pozT,char* spec,double inal,double viteza,double detenta,double procForma);
	Jucator(const Jucator &j);
	Jucator(char* pozT,char* spec,double inal,double vit,double detenta,double procent,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl);
	~Jucator();

	int getNrJucatori();
	void setNrJucatori(int nr);
	char* getPozitieTeren();
	void setPozitieTeren(char *pozTeren);
	char* getSpecialitatea();
	void setSpecialitatea(char *specialitatea);
	double getInaltime();
	void setInaltime(double inal);
	double getViteza();
	void setViteza(double vit);
	double getDetenta();
	void setDetenta(double detenta);
	double getForma();
	void setForma(double procent);

	void citire();
	void citire1();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

class Antrenor:public AngajatEchipa
{
	static int nrAntrenori;
protected:
	char* stilDeJoc;
	int aniExperienta;
	int campionateCastigate;
public:
	Antrenor();
	Antrenor(char* stil,int aniExp,int cC);
	Antrenor(const Antrenor &ant);
	Antrenor(char* stil,int aniExp,int cC,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl);
	~Antrenor();

	int getNrAntrenori();
	void setNrAntrenori(int nr);
	char* getStil();
	void setStil(char* stil);
	int getExperienta();
	void setExperienta(int aniExp);
	int getCampionateCastigate();
	void setCampionateCastigate(int campCas);

	void citire();
	void citire1();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

class Medic:public AngajatEchipa
{
	static int nrMedici;
protected:
	int experienta;
	char* facultateAbsolvita;
public:
	Medic();
	Medic(int exp,char* graduate);
	Medic(const Medic &m);
	Medic(int exp,char* graduate,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl);
	~Medic();

	int getNrMedici();
	void setNrMedici(int nr);
	int getExperienta();
	void setExperienta(int exp);
	char* getAbsolvire();
	void setAbsolvire(char* facultate);

	void citire();
	void citire1();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

class RandamentJucator:public Jucator
{
protected:
	int goluri;
	int assist;
	double procentPaseReusite;
	int cartonaseGalbene;
	int cartonaseRosii;
	int paseTotal;
public:
	RandamentJucator() : goluri(0),assist(0),procentPaseReusite(0),cartonaseGalbene(0),cartonaseRosii(0),paseTotal(0) {};
	RandamentJucator(int gol,int as,double proc,int cG,int cR,int pase);
	RandamentJucator(const RandamentJucator &ran);
	RandamentJucator(int gol,int as,double proc,int cG,int cR,int pase,char* pozT,char* spec,double inal,double vit,double detenta,double procent,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl);
	~RandamentJucator(){};

	int getGoluri();
	void setGoluri(int gol);
	int getAssist();
	void setAssist(int as);
	double getProcent();
	void setProcent(double proc);
	int getCG();
	void setCG(int cg);
	int getCR();
	void setCR(int cr);
	int getPase();
	void setPase(int pas);

	void citire();
	void citire1();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

void afisareInformatii(AngajatEchipa **ang,int nrAngajati,char *nume,ostream& out);
int pozitieAngajat(AngajatEchipa **ang,int nrAngajati,char *nume);
bool apartine(AngajatEchipa **ang,int nrAngajati,char *nume);
void Demitere(AngajatEchipa **&ang,int &nrAngajati,char *nume);
template <class T>
bool apartineAng(AngajatEchipa **ang,int nrAngajati,T *a);
void angajareAntrenor(AngajatEchipa **&ang,int &nrAngajati,Antrenor *a);
void angajareMedic(AngajatEchipa **&ang,int &nrAngajati,Medic *m);
void angajareJucator(AngajatEchipa **&ang,int &nrAngajati,RandamentJucator *ran);
