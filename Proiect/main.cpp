#include<iostream>
#include<string.h>
//#include<conio.h>
#include<fstream>
#include<stdio.h>
#include "Angajat.h"
#include "echipa.h"
using namespace std;
int main()
{
	ifstream fin("citire.in");
	Echipa::eroare(!fin.is_open(),"Eroare la deschidere!");

	cout<<"Informatiile despre echipa sunt citite din urmatoarele fisiere:"<<endl;
	cout<<"1)citire.in-Sunt citite informatiile generale despre echipa, stadion si meciurile jucate de aceasta."<<endl;
	cout<<"2)citire1.in-Sunt informatiile aferente la numarul de angajati ai echipei si de tipul fiecarui angajat(antrenor,jucator,medic)"<<endl;
	cout<<"3)antrenor.in-Sunt citite informatiile cu privire la antrenorii echipei."<<endl;
	cout<<"4)medici.in-Sunt citite informatiile cu privire la medicii echipei."<<endl;
	cout<<"5)jucatori.in-Sunt citite informatiile cu privire la jucatorii echipei."<<endl;
	cout<<"OBS: Pentru a modifica informatiile despre echipa accesati fisierele de mai sus si modificatile corespunzator."<<endl;

	Echipa *e=new Echipa();
	e->citire(fin);
	Stadion *s=new Stadion();
	s->citire(fin);
	Meciuri *meci=new Meciuri();
	meci->citire(fin);

	ofstream out("date.out");
	Echipa::eroare(!out.is_open(),"Eroare la deschidere!");

	e->afisare(out);
	meci->afisare(out);
	out<<"Nume stadion:"<<s->getNumeStadion()<<endl;

	int nrAngajati;
	int index,tip;

	ifstream fin1("citire1.in");
	Echipa::eroare(!fin1.is_open(),"Eroare la deschidere!");

	fin1>>nrAngajati;

	ifstream ant("antrenor.in");
	Echipa::eroare(!ant.is_open(),"Eroare la deschidere!");

	ifstream medic("medici.in");
	Echipa::eroare(!medic.is_open(),"Eroare la deschidere!");

	ifstream juc("jucatori.in");
	Echipa::eroare(!juc.is_open(),"Eroare la deschidere!");

	char nume[MAXN],stil[MAXN],facultate[MAXN],pozitie[MAXN],specialitate[MAXN];
	int varsta,cC,exp,zi,luna,an,inaltime,detenta,gol,ass,cG,cR,paseTotal;
	double salariu,viteza,procForma,procPase;
	Data *d;

	/*
	1-Jucator;
	2-Antrenor;
	3-Medic;
	*/

	AngajatEchipa **ang=new AngajatEchipa*[nrAngajati];
	for(index=0;index<nrAngajati;++index)
	{
		fin1>>tip;
		switch(tip)
		{
		case 1:
			{
				juc.getline(nume,MAXN);
				juc>>varsta;
				juc>>salariu;
				juc>>zi;
				juc>>luna;
				juc>>an;
				juc.getline(pozitie,MAXN);
				juc.getline(specialitate,MAXN);
				juc>>inaltime;
				juc>>viteza;
				juc>>detenta;
				juc>>procForma;
				juc>>gol;
				juc>>ass;
				juc>>procPase;
				juc>>cG;
				juc>>cR;
				juc>>paseTotal;
				d=new Data(zi,luna,an);
				ang[index]=new RandamentJucator(gol,ass,procPase,cG,cR,paseTotal,pozitie,specialitate,inaltime,viteza,detenta,procForma,"Jucator",salariu,d,nume,varsta,e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
				break;
			}
		case 2:
			{
				ant.getline(nume,MAXN);
				ant>>varsta;
				ant>>salariu;
				ant>>zi;
				ant>>luna;
				ant>>an;
				ant.getline(stil,MAXN);
				ant>>exp;
				ant>>cC;
				d=new Data(zi,luna,an);
				ang[index]=new Antrenor(stil,exp,cC,"Antrenor",salariu,d,nume,varsta,e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
				break;
			}
		case 3:
			{
				medic.getline(nume,MAXN);
				medic>>varsta;
				medic>>salariu;
				medic>>zi;
				medic>>luna;
				medic>>an;
				medic.getline(facultate,MAXN);
				medic>>exp;
				d=new Data(zi,luna,an);
				ang[index]=new Medic(exp,facultate,"Medic",salariu,d,nume,varsta,e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
				break;
			}
		}
	}

	out<<"Antrenori:"<<endl;
	for(index=0;index<nrAngajati;++index)
	{
		if(strcmp(ang[index]->getOcupatie(),"Antrenor")==0)
		{
			ang[index]->Persoana::afisare(out);
		}
	}

	out<<"Medici:"<<endl;
	for(index=0;index<nrAngajati;++index)
	{
		if(strcmp(ang[index]->getOcupatie(),"Medic")==0)
		{
			ang[index]->Persoana::afisare(out);
		}
	}

	out<<"Jucatori:"<<endl;
	for(index=0;index<nrAngajati;++index)
	{
		if(strcmp(ang[index]->getOcupatie(),"Jucator")==0)
		{
			ang[index]->Persoana::afisare(out);
		}
	}

	int operatie;
	char *nume1;

	ofstream afis("prelucrare.out");
	Echipa::eroare(!afis.is_open(),"Eroare la deschidere!");

	cout<<endl<<"Ce operatie doriti sa efectuati:"<<endl;
	cout<<"0-Oprire program."<<endl;
	cout<<"1)Afisare informatii aferente referitor la un angajat anume(criteriul de cautare este numele)."<<endl;
	cout<<"2)Demiterea unui angajati al echipei."<<endl;
	cout<<"3)Angajeaza un antrenor/medic/jucator."<<endl;
	cout<<"OBS:In urma operatiei efectuate va fi afisata noua componenta a echipei in fisierul prelucrare.out."<<endl;
	cout<<"Operatia:";
	cin>>operatie;

	Antrenor *coach=new Antrenor();
	Antrenor *c1;
	Medic *m=new Medic();
	Medic *m1;
	RandamentJucator *ran=new RandamentJucator();
	RandamentJucator *ran1;
	AngajatEchipa *angajat;

	do{
		switch(operatie)
		{
		case 0:
			{
				cout<<"Oprire program.";
				exit(EXIT_FAILURE);
			}
		case 1:
			{
				nume1=Echipa::citireSir("Dati numele angajatului:");
				afisareInformatii(ang,nrAngajati,nume1,afis);
				afis<<endl;
				break;
			}
		case 2:
			{
				nume1=Echipa::citireSir("Dati numele angajatului:");
				Demitere(ang,nrAngajati,nume1);
				e->afisare(afis);
				meci->afisare(afis);
				afis<<"Nume stadion:"<<s->getNumeStadion()<<endl;
				afis<<"Antrenori:"<<endl;
				for(index=0;index<nrAngajati;++index)
				{
					if(strcmp(ang[index]->getOcupatie(),"Antrenor")==0)
					{
						ang[index]->Persoana::afisare(afis);
					}
				}

				afis<<"Medici:"<<endl;
				for(index=0;index<nrAngajati;++index)
				{
					if(strcmp(ang[index]->getOcupatie(),"Medic")==0)
					{
						ang[index]->Persoana::afisare(afis);
					}
				}

				afis<<"Jucatori:"<<endl;
				for(index=0;index<nrAngajati;++index)
				{
					if(strcmp(ang[index]->getOcupatie(),"Jucator")==0)
					{
						ang[index]->Persoana::afisare(afis);
					}
				}
				afis<<endl;
				break;
			}
		case 3:
			{
				angajat=new AngajatEchipa();
				angajat->citire();
				if(strcmp(angajat->getOcupatie(),"Antrenor")==0 || strcmp(angajat->getOcupatie(),"antrenor")==0)
				{
					coach->citire1();
					c1=new Antrenor(coach->getStil(),coach->getExperienta(),coach->getCampionateCastigate(),angajat->getOcupatie(),angajat->getSalariu(),angajat->getData(),angajat->getNume(),angajat->getVarsta(),e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
					angajareAntrenor(ang,nrAngajati,c1);
					e->afisare(afis);
					meci->afisare(afis);
					afis<<"Nume stadion:"<<s->getNumeStadion()<<endl;
					afis<<"Antrenori:"<<endl;
					for(index=0;index<nrAngajati;++index)
					{
						if(strcmp(ang[index]->getOcupatie(),"Antrenor")==0)
						{
							ang[index]->Persoana::afisare(afis);
						}
					}

					afis<<"Medici:"<<endl;
					for(index=0;index<nrAngajati;++index)
					{
						if(strcmp(ang[index]->getOcupatie(),"Medic")==0)
						{
							ang[index]->Persoana::afisare(afis);
						}
					}

					afis<<"Jucatori:"<<endl;
					for(index=0;index<nrAngajati;++index)
					{
						if(strcmp(ang[index]->getOcupatie(),"Jucator")==0)
						{
							ang[index]->Persoana::afisare(afis);
						}
					}
				}
				else
				{
					if(strcmp(angajat->getOcupatie(),"Medic")==0 || strcmp(angajat->getOcupatie(),"medic")==0)
					{
						m->citire1();
						m1=new Medic(m->getExperienta(),m->getAbsolvire(),angajat->getOcupatie(),angajat->getSalariu(),angajat->getData(),angajat->getNume(),angajat->getVarsta(),e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
						angajareMedic(ang,nrAngajati,m1);
						e->afisare(afis);
						meci->afisare(afis);
						afis<<"Nume stadion:"<<s->getNumeStadion()<<endl;
						afis<<"Antrenori:"<<endl;
						for(index=0;index<nrAngajati;++index)
						{
							if(strcmp(ang[index]->getOcupatie(),"Antrenor")==0)
							{
								ang[index]->Persoana::afisare(afis);
							}
						}

						afis<<"Medici:"<<endl;
						for(index=0;index<nrAngajati;++index)
						{
							if(strcmp(ang[index]->getOcupatie(),"Medic")==0)
							{
								ang[index]->Persoana::afisare(afis);
							}
						}

						afis<<"Jucatori:"<<endl;
						for(index=0;index<nrAngajati;++index)
						{
							if(strcmp(ang[index]->getOcupatie(),"Jucator")==0)
							{
								ang[index]->Persoana::afisare(afis);
							}
						}
					}
					else
					{
						if(strcmp(angajat->getOcupatie(),"Jucator")==0 || strcmp(angajat->getOcupatie(),"jucator")==0)
						{
							ran->citire1();
							ran1=new RandamentJucator(ran->getGoluri(),ran->getAssist(),ran->getProcent(),ran->getCG(),ran->getCR(),ran->getPase(),ran->getPozitieTeren(),ran->getSpecialitatea(),ran->getInaltime(),ran->getViteza(),ran->getDetenta(),ran->getForma(),angajat->getOcupatie(),angajat->getSalariu(),angajat->getData(),angajat->getNume(),angajat->getVarsta(),e->getNumeEchipa(),e->getNumeCampionat(),e->getBuget(),e->getPuncte(),e->getPozitieClasament());
							angajareJucator(ang,nrAngajati,ran1);
							e->afisare(afis);
							meci->afisare(afis);
							afis<<"Nume stadion:"<<s->getNumeStadion()<<endl;
							afis<<"Antrenori:"<<endl;
							for(index=0;index<nrAngajati;++index)
							{
								if(strcmp(ang[index]->getOcupatie(),"Antrenor")==0)
								{
									ang[index]->Persoana::afisare(afis);
								}
							}

							afis<<"Medici:"<<endl;
							for(index=0;index<nrAngajati;++index)
							{
								if(strcmp(ang[index]->getOcupatie(),"Medic")==0)
								{
									ang[index]->Persoana::afisare(afis);
								}
							}

							afis<<"Jucatori:"<<endl;
							for(index=0;index<nrAngajati;++index)
							{
								if(strcmp(ang[index]->getOcupatie(),"Jucator")==0)
								{
									ang[index]->Persoana::afisare(afis);
								}
							}
						}
					}
				}
			}
		}
		cout<<"Operatie:";
		cin>>operatie;
	}while(operatie!=0);

	//_getch();
	return 0;
}
