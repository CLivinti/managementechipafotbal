#include<iostream>
#include<string.h>
#include "persoana.h"
#include"echipa.h"
using namespace std;

Data::Data(int zi,int luna,int an)
{
	this->zi=zi;
	this->luna=luna;
	this->an=an;
}

Data::Data(const Data &d)
{
	this->zi=d.zi;
	this->luna=d.luna;
	this->an=d.an;
}

int Data::getAn()
{
	return an;
}

int Data::getLuna()
{
	return luna;
}

int Data::getZi()
{
	return zi;
}

Data* Data::citire()
{
	Data *d=nullptr;
	int z,a,l;
	cout<<"Anul angajarii:";
	cin>>a;
	if(a%4==0 && ((a%100!=0) || (a%400==0)))
	{
		do{
			cout<<"Luna Angajarii:";
			cin>>l;
			if(l==2)
			{
				do{
					cout<<"Ziua angajarii:";
					cin>>z;
				}while(z<1 || z>29);
			}
			else
			{
				if( l==1 || l==3 || l==5 || l==7 || l==8 || l==10 || l==12)
				{
					do{
						cout<<"Ziua angajarii:";
						cin>>z;
					}while(z<1 || z>31);
				}
				else
				{
					do{
						cout<<"Ziua angajarii:";
						cin>>z;
					}while(z<0 || z>30);
				}
			}
		}while(l<1 || l>12);
	}
	else
	{
		do{
			cout<<"Luna Angajarii:";
			cin>>l;
			if(l==2)
			{
				do{
					cout<<"Ziua angajarii:";
					cin>>z;
				}while(z<1 || z>28);
			}
			else
			{
				if( l==1 || l==3 || l==5 || l==7 || l==8 || l==10 || l==12)
				{
					do{
						cout<<"Ziua angajarii:";
						cin>>z;
					}while(z<1 || z>31);
				}
				else
				{
					do{
						cout<<"Ziua angajarii:";
						cin>>z;
					}while(z<0 || z>30);
				}
			}
		}while(l<1 || l>12);
	}
	d=new Data(z,l,a);
	return d;
}

Persoana::Persoana(char *n,int v)
{
	this->nume=Echipa::copiereSir(n);
	this->varsta=v;
}

Persoana::Persoana(const Persoana &p)
{
	this->nume=Echipa::copiereSir(p.nume);
	this->varsta=p.varsta;
}

Persoana::~Persoana()
{
	Echipa::uitaSir(nume);
	varsta=0;
}

char* Persoana::getNume()
{
	return nume;
}

void Persoana::setNume(char* n)
{
	this->nume=Echipa::copiereSir(n);
}

int Persoana::getVarsta()
{
	return varsta;
}

void Persoana::setVarsta(int v)
{
	this->varsta=v;
}

void Persoana::citire()
{
	char *n;
	n=Echipa::citireSir("Nume:");
	setNume(n);
	int v;
	cout<<"Varsta:";
	cin>>v;
	setVarsta(v);
}

void Persoana::citire(istream &fin)
{
	char n[MAXN];
	fin.getline(n,MAXN);
	setNume(n);
	fin>>varsta;
}

void Persoana::afisare()
{
	cout<<this->nume<<"\t"<<this->varsta<<endl;
}

void Persoana::afisare(ostream &fout)
{
	fout<<this->nume<<"\t"<<this->varsta<<endl;
}