#include<iostream>
#include<string.h>
#include "Angajat.h"
using namespace std;

int Jucator::nrJucatori=0;
int Antrenor::nrAntrenori=0;
int Medic::nrMedici=0;

Jucator::Jucator()
{
	pozitieTeren=nullptr;
	specialitatea=nullptr;
	inaltime=0;
	viteza=0;
	detentaInaltime=0;
	procentForma=0;
	++nrJucatori;
}

Jucator::Jucator(char* pozT,char* spec,double inal,double viteza,double detenta,double procForma)
{
	setPozitieTeren(pozT);
	setSpecialitatea(spec);
	setInaltime(inal);
	setViteza(viteza);
	setDetenta(detenta);
	setForma(procForma);
	++nrJucatori;
}

Jucator::Jucator(const Jucator &j)
	:AngajatEchipa(j.ocupatie,j.salariu,j.dataAngajarii,j.nume,j.varsta,j.numeEchipa,j.numeCampionat,j.buget,j.punctaj,j.pozitieClasament)
{
	setPozitieTeren(j.pozitieTeren);
	setSpecialitatea(j.specialitatea);
	setInaltime(j.inaltime);
	setViteza(j.viteza);
	setDetenta(j.detentaInaltime);
	setForma(j.procentForma);
	++nrJucatori;
}

Jucator::Jucator(char* pozT,char* spec,double inal,double vit,double detenta,double procent,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl)
	:AngajatEchipa(poz,sal,d,nume,varsta,nE,nC,bug,punctaj,pozCl)
{
	setPozitieTeren(pozT);
	setSpecialitatea(spec);
	setInaltime(inal);
	setViteza(vit);
	setDetenta(detenta);
	setForma(procent);
	++nrJucatori;
}

Jucator::~Jucator()
{
	uitaSir(pozitieTeren);
	uitaSir(specialitatea);
	--nrJucatori;
}

int Jucator::getNrJucatori()
{
	return nrJucatori;
}

void Jucator::setNrJucatori(int nr)
{
	this->nrJucatori=nr;
}

char* Jucator::getPozitieTeren()
{
	return pozitieTeren;
}

void Jucator::setPozitieTeren(char* pozTeren)
{
	pozitieTeren=copiereSir(pozTeren);
}

char* Jucator::getSpecialitatea()
{
	return specialitatea;
}

void Jucator::setSpecialitatea(char* spec)
{
	specialitatea=copiereSir(spec);
}

double Jucator::getInaltime()
{
	return inaltime;
}

void Jucator::setInaltime(double inal)
{
	this->inaltime=inal;
}

double Jucator::getViteza()
{
	return viteza;
}

void Jucator::setViteza(double viteza)
{
	this->viteza=viteza;
}

double Jucator::getDetenta()
{
	return detentaInaltime;
}

void Jucator::setDetenta(double detenta)
{
	this->detentaInaltime=detenta;
}

double Jucator::getForma()
{
	return procentForma;
}

void Jucator::setForma(double proc)
{
	this->procentForma=proc;
}

void Jucator::citire()
{
	AngajatEchipa::citire();
	char *pozT,*spec;
	double inal,vit,detenta,proc;
	pozT=Echipa::citireSir("Pozitie in teren:");
	setPozitieTeren(pozT);
	do{
		spec=Echipa::citireSir("Specialitatea:");
		setSpecialitatea(spec);
	}while(strcmp(spec,"Tehnic")!=0 && strcmp(spec,"Puternic")!=0 && strcmp(spec,"Cap")!=0 && strcmp(spec,"Rapid")!=0 && strcmp(spec,"Imprevizibil")!=0 && strcmp(spec,"tehnic")!=0 && strcmp(spec,"cap")!=0 && strcmp(spec,"puternic")!=0 && strcmp(spec,"rapid")!=0 && strcmp(spec,"imprevizibil")!=0);
	do
	{
		cout<<"Inaltime:";
		cin>>inal;
		setInaltime(inal);
	}while(inaltime<=100);
	do
	{
		cout<<"Viteza:";
		cin>>vit;
		setViteza(vit);
	}while(viteza<=0);
	do
	{
		cout<<"Detenta inaltime:";
		cin>>detenta;
		setDetenta(detenta);
	}while(detentaInaltime<=0);
	do
	{
		cout<<"Forma(Procentaj):";
		cin>>proc;
		setForma(proc);
	}while(procentForma<0 || procentForma >100);
}

void Jucator::citire1()
{
	char *pozT,*spec;
	double inal,vit,detenta,proc;
	pozT=Echipa::citireSir("Pozitie in teren:");
	setPozitieTeren(pozT);
	spec=Echipa::citireSir("Specialitatea:");
	setSpecialitatea(spec);
	do
	{
		cout<<"Inaltime:";
		cin>>inal;
		setInaltime(inal);
	}while(inaltime<=100);
	do
	{
		cout<<"Viteza:";
		cin>>vit;
		setViteza(vit);
	}while(viteza<=0);
	do
	{
		cout<<"Detenta inaltime:";
		cin>>detenta;
		setDetenta(detenta);
	}while(detentaInaltime<=0);
	do
	{
		cout<<"Forma(Procentaj):";
		cin>>proc;
		setForma(proc);
	}while(procentForma<0 || procentForma >100);
}

void Jucator::citire(istream &fin)
{
	AngajatEchipa::citire(fin);
	pozitieTeren=citireSir(fin);
	specialitatea=citireSir(fin);
	fin>>inaltime;
	fin.get();
	fin>>viteza;
	fin.get();
	fin>>detentaInaltime;
	fin.get();
	fin>>procentForma;
	fin.get();
}

void Jucator::afisare()
{
	AngajatEchipa::afisare();
	cout<<"Pozitie:"<<this->pozitieTeren<<endl;
	cout<<"Specialitatea:"<<this->specialitatea<<endl;
	cout<<"Inaltimea:"<<this->inaltime<<endl;
	cout<<"Viteza:"<<this->viteza<<endl;
	cout<<"Detenta Inaltime:"<<this->detentaInaltime<<endl;
	cout<<"Procent Forma:"<<this->procentForma<<endl;
}

void Jucator::afisare(ostream &fout)
{
	AngajatEchipa::afisare(fout);
	fout<<"Pozitie:"<<this->pozitieTeren<<endl;
	fout<<"Specialitatea:"<<this->specialitatea<<endl;
	fout<<"Inaltimea:"<<this->inaltime<<endl;
	fout<<"Viteza:"<<this->viteza<<endl;
	fout<<"Detenta Inaltime:"<<this->detentaInaltime<<endl;
	fout<<"Procent Forma:"<<this->procentForma<<endl;
}

Antrenor::Antrenor()
{
	stilDeJoc=nullptr;
	aniExperienta=0;
	campionateCastigate=0;
	++nrAntrenori;
}

Antrenor::Antrenor(char* stil,int aniExp,int cC)
{
	setStil(stil);
	setExperienta(aniExp);
	setCampionateCastigate(cC);
	++nrAntrenori;
}

Antrenor::Antrenor(const Antrenor &a)
	:AngajatEchipa(a.ocupatie,a.salariu,a.dataAngajarii,a.nume,a.varsta,a.numeEchipa,a.numeCampionat,a.buget,a.punctaj,a.pozitieClasament)
{
	setStil(a.stilDeJoc);
	setExperienta(a.aniExperienta);
	setCampionateCastigate(a.campionateCastigate);
	++nrAntrenori;
}

Antrenor::Antrenor(char* stil,int aniExp,int cC,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl)
	:AngajatEchipa(poz,sal,d,nume,varsta,nE,nC,bug,punctaj,pozCl)
{
	setStil(stil);
	setExperienta(aniExp);
	setCampionateCastigate(cC);
	++nrAntrenori;
}

Antrenor::~Antrenor()
{
	uitaSir(stilDeJoc);
	--nrAntrenori;
}

int Antrenor::getNrAntrenori()
{
	return nrAntrenori;
}

void Antrenor::setNrAntrenori(int nr)
{
	this->nrAntrenori=nr;
}

char* Antrenor::getStil()
{
	return stilDeJoc;
}

void Antrenor::setStil(char* stil)
{
	this->stilDeJoc=copiereSir(stil);
}

int Antrenor::getExperienta()
{
	return aniExperienta;
}

void Antrenor::setExperienta(int aniExp)
{
	this->aniExperienta=aniExp;
}

int Antrenor::getCampionateCastigate()
{
	return campionateCastigate;
}

void Antrenor::setCampionateCastigate(int cC)
{
	this->campionateCastigate=cC;
}

void Antrenor::citire()
{
	AngajatEchipa::citire();
	char *stil;
	int exp,cC;
	do{
		stil=Echipa::citireSir("Stil de joc:");
		setStil(stil);
	}while(strcmp(this->stilDeJoc,"Ofensiv")!=0 && strcmp(this->stilDeJoc,"Defensiv")!=0);

	do{
		cout<<"Experienta:";
		cin>>exp;
		setExperienta(exp);
	}while(this->aniExperienta<0);

	do{
		cout<<"Campionate castigate:";
		cin>>cC;
		setCampionateCastigate(cC);
	}while(this->campionateCastigate<0);
}

void Antrenor::citire1()
{
	char *stil;
	int exp,cC;
	do{
		stil=Echipa::citireSir("Stil de joc:");
		setStil(stil);
	}while(strcmp(this->stilDeJoc,"Ofensiv")!=0 && strcmp(this->stilDeJoc,"Defensiv")!=0);

	do{
		cout<<"Experienta:";
		cin>>exp;
		setExperienta(exp);
	}while(this->aniExperienta<0);

	do{
		cout<<"Campionate castigate:";
		cin>>cC;
		setCampionateCastigate(cC);
	}while(this->campionateCastigate<0);
}

void Antrenor::citire(istream &fin)
{
	AngajatEchipa::citire(fin);
	stilDeJoc=citireSir(fin);
	fin>>aniExperienta;
	fin.get();
	fin>>campionateCastigate;
	fin.get();
}

void Antrenor::afisare()
{
	AngajatEchipa::afisare();
	cout<<"Stil de joc:"<<this->stilDeJoc<<endl;
	cout<<"Experienta:"<<this->aniExperienta<<endl;
	cout<<"Campionate castigate:"<<this->campionateCastigate<<endl;
}

void Antrenor::afisare(ostream &fout)
{
	AngajatEchipa::afisare(fout);
	fout<<"Stil de joc:"<<this->stilDeJoc<<endl;
	fout<<"Experienta:"<<this->aniExperienta<<endl;
	fout<<"Campionate castigate:"<<this->campionateCastigate<<endl;
}

Medic::Medic()
{
	experienta=0;
	facultateAbsolvita=nullptr;
	++nrMedici;
}

Medic::Medic(int exp,char* facultate)
{
	setExperienta(exp);
	setAbsolvire(facultate);
	++nrMedici;
}

Medic::Medic(const Medic &m)
	:AngajatEchipa(m.ocupatie,m.salariu,m.dataAngajarii,m.nume,m.varsta,m.numeEchipa,m.numeCampionat,m.buget,m.punctaj,m.pozitieClasament)
{
	setExperienta(m.experienta);
	setAbsolvire(m.facultateAbsolvita);
	++nrMedici;
}

Medic::Medic(int exp,char* graduate,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl)
	:AngajatEchipa(poz,sal,d,nume,varsta,nE,nC,bug,punctaj,pozCl)
{
	setExperienta(exp);
	setAbsolvire(graduate);
	++nrMedici;
}

Medic::~Medic()
{
	uitaSir(facultateAbsolvita);
	--nrMedici;
}

int Medic::getNrMedici()
{
	return nrMedici;
}

void Medic::setNrMedici(int nr)
{
	this->nrMedici=nr;
}

int Medic::getExperienta()
{
	return experienta;
}

void Medic::setExperienta(int exp)
{
	this->experienta=exp;
}

char* Medic::getAbsolvire()
{
	return facultateAbsolvita;
}

void Medic::setAbsolvire(char* facultate)
{
	this->facultateAbsolvita=copiereSir(facultate);
}
void Medic::citire()
{
	AngajatEchipa::citire();
	int exp;
	char *fac;
	fac=Echipa::citireSir("Facultate absolvita:");
	setAbsolvire(fac);
	do{
		cout<<"Experienta:";
		cin>>exp;
		setExperienta(exp);
	}while(this->experienta<0);
}

void Medic::citire1()
{
	int exp;
	char *fac;
	fac=Echipa::citireSir("Facultate absolvita:");
	setAbsolvire(fac);
	do{
		cout<<"Experienta:";
		cin>>exp;
		setExperienta(exp);
	}while(this->experienta<0);
}

void Medic::citire(istream &fin)
{
	AngajatEchipa::citire(fin);
	facultateAbsolvita=citireSir(fin);
	fin>>experienta;
	fin.get();
}

void Medic::afisare()
{
	AngajatEchipa::afisare();
	cout<<"Experienta:"<<this->experienta<<endl;
	cout<<"Facultate absolvita:"<<this->facultateAbsolvita<<endl;
}

void Medic::afisare(ostream &fout)
{
	AngajatEchipa::afisare(fout);
	fout<<"Experienta:"<<this->experienta<<endl;
	fout<<"Facultate absolvita:"<<this->facultateAbsolvita<<endl;
}

RandamentJucator::RandamentJucator(int gol,int as,double proc,int cG,int cR,int pase)
{
	setGoluri(gol);
	setAssist(as);
	setProcent(proc);
	setCG(cG);
	setCR(cR);
	setPase(pase);
}

RandamentJucator::RandamentJucator(const RandamentJucator &ran)
	:Jucator(ran.pozitieTeren,ran.specialitatea,ran.inaltime,ran.viteza,ran.detentaInaltime,ran.procentForma,ran.ocupatie,ran.salariu,ran.dataAngajarii,ran.nume,ran.varsta,ran.numeEchipa,ran.numeCampionat,ran.buget,ran.punctaj,ran.pozitieClasament)
{
	setGoluri(ran.goluri);
	setAssist(ran.assist);
	setProcent(ran.procentForma);
	setCG(ran.cartonaseGalbene);
	setCR(ran.cartonaseRosii);
	setPase(ran.paseTotal);
}

RandamentJucator::RandamentJucator(int gol,int as,double proc,int cG,int cR,int pase,char* pozT,char* spec,double inal,double vit,double detenta,double procent,char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl)
	:Jucator(pozT,spec,inal,vit,detenta,procent,poz,sal,d,nume,varsta,nE,nC,bug,punctaj,pozCl)
{
	setGoluri(gol);
	setAssist(as);
	setProcent(proc);
	setCG(cG);
	setCR(cR);
	setPase(pase);
}

int RandamentJucator::getGoluri()
{
	return goluri;
}

void RandamentJucator::setGoluri(int gol)
{
	this->goluri=gol;
}

int RandamentJucator::getAssist()
{
	return assist;
}

void RandamentJucator::setAssist(int as)
{
	this->assist=as;
}

double RandamentJucator::getProcent()
{
	return procentPaseReusite;
}

void RandamentJucator::setProcent(double proc)
{
	this->procentPaseReusite=proc;
}

int RandamentJucator::getCG()
{
	return cartonaseGalbene;
}

void RandamentJucator::setCG(int cg)
{
	this->cartonaseGalbene=cg;
}

int RandamentJucator::getCR()
{
	return cartonaseRosii;
}

void RandamentJucator::setCR(int cr)
{
	this->cartonaseRosii=cr;
}

int RandamentJucator::getPase()
{
	return paseTotal;
}

void RandamentJucator::setPase(int pase)
{
	this->paseTotal=pase;
}

void RandamentJucator::citire()
{
	Jucator::citire();
	int gol,ass,cG,cR,pT;
	double proc;
	cout<<"Goluri:";
	cin>>gol;
	setGoluri(gol);
	cout<<"Assist-uri:";
	cin>>ass;
	setAssist(ass);
	cout<<"Procent pase reusite:";
	cin>>proc;
	setProcent(proc);
	cout<<"Cartonase Galbene:";
	cin>>cG;
	setCG(cG);
	cout<<"Cartonase Rosii:";
	cin>>cR;
	setCR(cR);
	cout<<"Pase in total:";
	cin>>pT;
	setPase(pT);
}

void RandamentJucator::citire1()
{
	Jucator::citire1();
	int gol,ass,cG,cR,pT;
	double proc;
	cout<<"Goluri:";
	cin>>gol;
	setGoluri(gol);
	cout<<"Assist-uri:";
	cin>>ass;
	setAssist(ass);
	cout<<"Procent pase reusite:";
	cin>>proc;
	setProcent(proc);
	cout<<"Cartonase Galbene:";
	cin>>cG;
	setCG(cG);
	cout<<"Cartonase Rosii:";
	cin>>cR;
	setCR(cR);
	cout<<"Pase in total:";
	cin>>pT;
	setPase(pT);
}

void RandamentJucator::citire(istream &fin)
{
	Jucator::citire(fin);
	fin>>goluri;
	fin.get();
	fin>>assist;
	fin.get();
	fin>>paseTotal;
	fin.get();
	fin>>procentPaseReusite;
	fin.get();
	fin>>cartonaseGalbene;
	fin.get();
	fin>>cartonaseRosii;
	fin.get();
}

void RandamentJucator::afisare()
{
	Jucator::afisare();
	cout<<"Goluri:"<<this->goluri<<endl;
	cout<<"Assist-uri:"<<this->assist<<endl;
	cout<<"Pase total:"<<this->paseTotal<<endl;
	cout<<"Procent pase reusite:"<<this->procentPaseReusite<<endl;
	cout<<"Cartonase Galbene:"<<this->cartonaseGalbene<<endl;
	cout<<"Cartonase Rosii:"<<this->cartonaseRosii<<endl;
}

void RandamentJucator::afisare(ostream &fout)
{
	Jucator::afisare(fout);
	fout<<"Goluri:"<<this->goluri<<endl;
	fout<<"Assist-uri:"<<this->assist<<endl;
	fout<<"Pase total:"<<this->paseTotal<<endl;
	fout<<"Procent pase reusite:"<<this->procentPaseReusite<<endl;
	fout<<"Cartonase Galbene:"<<this->cartonaseGalbene<<endl;
	fout<<"Cartonase Rosii:"<<this->cartonaseRosii<<endl;
}

void afisareInformatii(AngajatEchipa **ang,int nrAngajati,char *nume,ostream& out)
{
	int index;
	for(index=0;index<nrAngajati;++index)
	{
		if(strcmp(ang[index]->getNume(),nume)==0)
		{
			ang[index]->afisare(out);
		}
	}
}

int pozitieAngajat(AngajatEchipa **ang,int nrAngajati,char *nume)
{
	for(int i=0;i<nrAngajati;++i)
	{
		if(strcmp(ang[i]->getNume(),nume)==0)
		{
			return i;
		}
	}
	return 0;
}

bool apartine(AngajatEchipa **ang,int nrAngajati,char *nume)
{
	for(int i=0;i<nrAngajati;++i)
	{
		if(strcmp(ang[i]->getNume(),nume)==0)
		{
			return 1;
		}
	}
	return 0;
}

void Demitere(AngajatEchipa **&ang,int &nrAngajati,char *nume)
{
	if(apartine(ang,nrAngajati,nume))
	{
		int poz=pozitieAngajat(ang,nrAngajati,nume);
		int index;
		AngajatEchipa **ang1=nullptr;
		ang1=new AngajatEchipa*[nrAngajati-1];
		Echipa::eroare(!ang1,"Eroare la alocarea dinamica a memoriei.");
		for(index=0;index<poz;++index)
		{
			ang1[index]=ang[index];
		}
		for(index=poz+1;index<nrAngajati;++index)
		{
			ang1[index-1]=ang[index];
		}
		if(ang!=nullptr)
		{
			delete ang;
		}
		ang=ang1;
		--nrAngajati;
	}
	else
	{
		cout<<"Persoana pe care introdusa nu exista in lista angajatilor a acestei echipe."<<endl;
	}
}

template <class T>
bool apartineAng(AngajatEchipa **ang,int nrAngajati,T *a)
{
	for(int i=0;i<nrAngajati;++i)
	{
		if(strcmp(ang[i]->getNume(),a->getNume())==0 && ang[i]->getVarsta()==a->getVarsta() && strcmp(ang[i]->getOcupatie(),a->getOcupatie())==0)
			return 1;
	}
	return 0;
}


void angajareAntrenor(AngajatEchipa **&ang,int &nrAngajati,Antrenor *a)
{
	if(apartineAng(ang,nrAngajati,a))
	{
		cout<<"Antrenorul este deja un angajat al echipei."<<endl;
	}
	else
	{
		AngajatEchipa **ang1=nullptr;
		ang1=new AngajatEchipa*[++nrAngajati];
		Echipa::eroare(!ang1,"Eroare la alocare dinamica a memoriei.");

		for(int i=0;i<nrAngajati-1;++i)
		{
			ang1[i]=ang[i];
		}

		ang1[nrAngajati-1]=a;

		if(ang!=nullptr)
		{
			delete ang;
		}

		ang=ang1;
	}
}

void angajareMedic(AngajatEchipa **&ang,int &nrAngajati,Medic *m)
{
	if(apartineAng(ang,nrAngajati,m))
	{
		cout<<"Medicul este deja un angajat al echipei."<<endl;
	}
	else
	{
		AngajatEchipa **ang1=new AngajatEchipa*[++nrAngajati];
		Echipa::eroare(!ang1,"Eroare la alocarea dinamica a memoriei.");

		for(int i=0;i<nrAngajati-1;++i)
		{
			ang1[i]=ang[i];
		}

		ang1[nrAngajati-1]=m;

		if(ang!=nullptr)
		{
			delete ang;
		}

		ang=ang1;
	}
}

void angajareJucator(AngajatEchipa **&ang,int &nrAngajati,RandamentJucator *ran)
{
	if(apartineAng(ang,nrAngajati,ran))
	{
		cout<<"Jucatorul este deja un angajat al echipei."<<endl;
	}
	else
	{
		AngajatEchipa **ang1=new AngajatEchipa*[++nrAngajati];
		Echipa::eroare(!ang1,"Eroare la alocarea dinamica a memoriei!");

		for(int i=0;i<nrAngajati-1;++i)
		{
			ang1[i]=ang[i];
		}

		ang1[nrAngajati-1]=ran;

		if(ang!=nullptr)
		{
			delete ang;
		}

		ang=ang1;
	}
}