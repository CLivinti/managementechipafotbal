#pragma once

#include "persoana.h"
#include <fstream>
using namespace std;

#define MAXN 1000

class Echipa
{
	static int nrEchipe;
protected:
	char *numeEchipa;
	char *numeCampionat;
	double buget;
	int punctaj;
	int pozitieClasament;
public:
	Echipa();
	Echipa(char* nE,char* nC,double bug,int punc,int pozCl);
	Echipa(const Echipa &e);
	virtual ~Echipa();

	int getNrEchipe();
	void setNrEchipe(int nr);
	char* getNumeEchipa();
	void setNumeEchipa(char* numeE);
	char* getNumeCampionat();
	void setNumeCampionat(char* numeC);
	double getBuget();
	void setBuget(double bug);
	int getPuncte();
	void setPuncte(int punct);
	int getPozitieClasament();
	void setPozitieClasament(int pozCl);

	static void eroare(int cond,char *mesaj);
	static char* citireSir(char *mesaj);
	static int citireInt(char *mesaj);
	static double citireReal(char *mesaj);
	static void uitaSir(char *&sir);
	static char* copiereSir(char *sir);
	static char* citireSir(istream &fin);
	static int citireInt(istream &fin);

	virtual void citire();
	virtual void citire(istream &fin);
	virtual void afisare();
	virtual void afisare(ostream &fout);
};

class Stadion:public Echipa
{
	static int nrStadioane;
protected:
	char *numeStadion;
	int nrLocuri;
	double lungimeTeren;
	double latimeTeren;
public:
	Stadion();
	Stadion(char *nS,int nrL,double lung,double lat);
	Stadion(const Stadion &s);
	Stadion(char *nS,int nrL,double lung,double lat,char *nE,char* nC,double bug,int punc,int pozCl);
	~Stadion();

	int getNrStadioane();
	void setNrStadioane(int nr);
	char* getNumeStadion();
	void setNumeStadion(char* nS);
	int getLocuri();
	void setLocuri(int loc);
	double getLungime();
	void setLungime(double lung);
	double getLatime();
	void setLatime(double lat);

	void citire();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

class Meciuri:public Echipa
{
protected:
	int victorii;
	int egaluri;
	int infrangeri;
	int goluriMarcate;
	int goluriPrimite;
public:
	Meciuri() : victorii(0),egaluri(0),infrangeri(0),goluriMarcate(0),goluriPrimite(0) {};
	Meciuri(int vic,int egal,int infrangeri,int gM,int gP);
	Meciuri(const Meciuri &m);
	Meciuri(int vic,int egal,int infrangeri,int gM,int gP,char* nE,char* nC,double bug,int punc,int pozCl);
	~Meciuri(){};

	int getVictorii();
	void setVictorii(int vic);
	int getEgaluri();
	void setEgaluri(int egal);
	int getInfrangeri();
	void setInfrangeri(int infrang);
	int getGoluriM();
	void setGoluriM(int golM);
	int getGoluriP();
	void setGoluriP(int golP);

	void citire();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};

class AngajatEchipa:public Echipa,public Persoana
{
	static int nrAngajati;
protected:
	char *ocupatie;
	double salariu;
	Data *dataAngajarii;
public:
	AngajatEchipa();
	AngajatEchipa(char *pozEc,double sal,Data *d);
	AngajatEchipa(const AngajatEchipa &ang);
	AngajatEchipa(char *poz,double sal,Data *d,char *nume,int varsta,char *nE,char *nC,double bug,int punctaj,int pozCl);
	~AngajatEchipa();

	int getNrAngajati();
	void setNrAngajati(int nr);
	char *getOcupatie();
	void setOcupatie(char* oc);
	double getSalariu();
	void setSalariu(double sal);
	Data* getData();
	void setData(Data *d);

	void citire();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};