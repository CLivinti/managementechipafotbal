#pragma once

#include<fstream>
using namespace std;

class Data{
	int zi;
	int luna;
	int an;
public:
	Data() : zi(0), luna(0), an(0) {};
	Data(int zi,int luna,int an);
	Data(const Data &d);
	int getAn();
	int getLuna();
	int getZi();
	Data* citire();
};

class Persoana
{
protected:
	char* nume;
	int varsta;
public:
	Persoana() : nume(nullptr),varsta(0){};
	Persoana(char *n,int v);
	Persoana(const Persoana &p);
	~Persoana();
	char* getNume();
	void setNume(char* n);
	int getVarsta();
	void setVarsta(int v);
	void citire();
	void citire(istream &fin);
	void afisare();
	void afisare(ostream &fout);
};